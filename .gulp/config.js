'use strict';

var args = require('yargs').argv;
//var gutil = require('gulp-util');

if (!args.prod) {
  args.prod = false;
}

if (!args.debug) {
  args.debug = false;
}

// http://pugofka.com/blog/technology/the-prepared-starting-package-front-end-development-on-gulp/

var path = {
  build: { // Куда складывать готовые после сборки файлы
    html: 'build/',
    js: 'build/assets/js/',
    css: 'build/assets/css/',
    images: 'build/assets/images/',
    fonts: 'build/assets/fonts/',
    contentImages: 'build/images/',
    sprites: 'src/assets/images/',
    spritesCss: 'src/assets/sass/'
  },

  src: { //Пути, откуда брать исходники
    jade: 'src/jade/**/[!^_]*.jade',
    js: 'src/assets/js/*.js',
    jshint: 'src/js/*.js',
    sass: 'src/assets/sass/styles.scss',
    stylus: 'src/assets/stylus/styles.styl',
    images: 'src/assets/images/**/*.*',
    fonts: 'src/assets/fonts/**/*.*',
    contentImages: 'src/images/**/*.*',
    sprites: 'src/assets/sprites/**/*.png'
  },

  watch: {
    //html: 'build/**/*.html',
    js: 'build/assets/js/**/*.js',
    css: 'build/assets/css/**/*.css'
  },

  clean: './build', //директории которые могут очищаться
  outputDir: './build'
};

module.exports = {

  production: args.prod || false,

  debug: args.debug || false,

  revCharacters: '123456789defghijklmnopqrstuvwxyz',

  watch: {
    html: 'src/template/**/*.html',
    jade: 'src/jade/**/*.jade',
    js: 'src/assets/js/**/*.js',
    css: 'build/assets/css/**/*.css',
    sass: 'src/assets/sass/**/*.scss',
    images: 'src/assets/images/**/*.*',
    contentImages: 'src/images/**/*.*',
    fonts: 'src/assets/fonts/**/*.*',
    sprites: 'src/assets/sprites/*.png'
  },

  browsersync: {
    //proxy: 'sheerdecor',
    server: {
      baseDir: "build",
      index: "main.html"
    },
    port: 9003,
    //external: projectConfig.host,
    watchTask: true,
    files: [
      path.watch.css,
      path.watch.js,
      path.watch.html
    ],
    //logLevel: 'info',
    logConnections: true,
    logFileChanges: true,
    open: "external",
    ui: {
      port: 3007
    }
  },

  jade: {
    src: path.src.jade,
    dest: path.build.html,

    options: {
      pretty: true
    }
  },

  sass: {
    src: path.src.sass,
    dest: path.build.css,
    options: {
      outputStyle: 'nested',
      sourceComments: !args.prod,
      errLogToConsole: true
    }
  },

  stylus: {
    src: path.src.stylus,
    dest: path.build.css,
    options: {
      compress: false
    }
  },

  autoprefixer: {
    browsers: [
      'last 10 versions',
      'safari 5',
      'ie 8',
      'opera 12',
      'ios 5',
      'android 2.3',
      'Chrome 30',
      'Firefox 25',
      'BlackBerry 10'
    ],
    cascade: false
  },

  sprites: {
    src: path.src.sprites,
    dest: path.build.sprites,
    cssDest: path.build.spritesCss
  },

  size: {
    showFiles: true
  },

  optimize: {
    css: {
      src: path.build.css + '/*.css',
      dest: path.build.css,
      options: {
        keepSpecialComments: 0,
        compatibility: '*,-units.pc,-units.pt,+properties.urlQuotes'
      }
    },

    js: {
      src: path.src.js,
      dest: path.build.js
    },

    images: {
      src: path.src.images,
      dest: path.build.images,
      options: {
        progressive: true, //сжатие .jpg
        svgoPlugins: [{ removeViewBox: false }], //сжатие .svg
        interlaced: true, //сжатие .gif
        optimizationLevel: 3 //степень сжатия от 0 до 7
      }
    },

    contentImages: {
      src: path.src.contentImages,
      dest: path.build.contentImages
    }
  },

  base64: {
    src: path.build.css + '/*.css',
    dest: path.build.css,
    options: {
      //baseDir: 'build',
      extensions: ['gif', 'png', 'svg', /\.(jpg)#datauri$/i],
      exclude: ['sprite-', '#external'],
      maxImageSize: 6 * 1024, // bytes
      debug: false
    }
  },

  deploy: {
    localFilesGlob: [
      './TMP/**',
      './build/**',
      './src/**'
    ],
    remoteFolder: '/sheerdecor',
    options: {
      /*host: '46.36.218.142',
       user: 'mplus',
       password: 'expXN2yinj79',*/
      host: '46.36.218.142',
      user: 'mplus',
      password: 'expXN2yinj79',
      port: 21,
      parallel: 5,
      //log: gutil.log,
      debug: false
    }
  },

  deployProd: {
    localFilesGlob: ['./build/assets/**', '!./build/assets/**/*.map'],
    remoteFolder: '/sheerdecor.ru/public_html/wa-data/public',
    deletedFolders: '/sheerdecor.ru/public_html/wa-data/public/assets',
    srcOptions: {
      base: './build',
      buffer: false
    },
    options: {
      host: '193.176.79.121',
      user: 'oleg_www',
      //user: 'seosheks_oleg',
      password: '!&QRHfUBEs$',
      debug: true,
    }
  }
};
