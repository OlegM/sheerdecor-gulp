'use strict';

const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const concat = require('gulp-concat');

module.exports = function (config) {

  return function () {

    return gulp.src(config.optimize.js.src)
      .pipe($.plumber())
      .pipe(concat('theme.min.js'))
      //.pipe($.sourcemaps.init())
      .pipe($.uglify())
      //.pipe($.sourcemaps.write())
      .pipe(gulp.dest(config.optimize.js.dest));
  };
};

