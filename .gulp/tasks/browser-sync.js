'use strict';

var gulp          = require('gulp');
const browserSync = require('browser-sync');

/**
 * Run the build task and start a server with BrowserSync
 */
module.exports = function (config) {

    /*return function () {

     browserSync.init(config.browsersync);

     browserSync.watch(config.browsersync.files).
     on('change', browserSync.reload);
     };*/

    return function (cb) {
        browserSync(config.browsersync, cb);
    }
};
