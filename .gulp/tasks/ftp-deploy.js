'use strict';

const $    = require('gulp-load-plugins')();
var ftp    = require('vinyl-ftp');
const gulp = require('gulp');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function () {
        var conn = ftp.create(config.deploy.options);

        return gulp.src(config.deploy.localFilesGlob, {base: '.', buffer: false})
            .pipe($.plumber())
            .pipe(conn.newer(config.deploy.remoteFolder)) // only upload newer files
            .pipe(conn.dest(config.deploy.remoteFolder));
    };
};
