'use strict';

const $    = require('gulp-load-plugins')();
const ftp    = require('vinyl-ftp');
const gulp = require('gulp');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function () {

        var conn    = ftp.create(config.deploy.options);
        var watcher = gulp.watch(config.deploy.localFilesGlob, gulp.parallel('ftp-deploy'));

        watcher.on('change', function (event) {
            console.log('Changes detected! Uploading file "' + event + '", ');

            return gulp.src(event, {base: '.', buffer: false})
                .pipe($.plumber())
                .pipe(conn.newer(config.deploy.remoteFolder)) // only upload newer files
                .pipe(conn.dest(config.deploy.remoteFolder));
        });
    };
};
