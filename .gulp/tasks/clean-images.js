'use strict';

var gulp = require('gulp');
var del  = require('del');

module.exports = function (config) {

    var configImages        = config.optimize.images;
    var configContentImages = config.optimize.contentImages;

    return function () {
        return del([
            configImages.dest,
            configContentImages.dest
        ]);
    }
};
