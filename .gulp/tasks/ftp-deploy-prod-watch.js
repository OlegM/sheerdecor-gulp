'use strict';

const $    = require('gulp-load-plugins')();
const ftp    = require('vinyl-ftp');
const gulp = require('gulp');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function () {

        var conn    = ftp.create(config.deployProd.options);
        var watcher = gulp.watch(config.deployProd.localFilesGlob, gulp.parallel('ftp-deploy-prod'));

        watcher.on('change', function (event) {
            console.log('Changes detected! Uploading file "' + event + '", ');

            return gulp.src(event, config.deployProd.srcOptions)
                .pipe($.plumber())
                .pipe(conn.newer(config.deployProd.remoteFolder)) // only upload newer files
                .pipe(conn.dest(config.deployProd.remoteFolder));
        });
    };
};
