'use strict';

const $    = require('gulp-load-plugins')();
const gulp = require('gulp');

module.exports = function (config) {

    var configImages = config.optimize.images;

    return function () {
        // Оптимизация статичных изображений (для верстки)
        return gulp.src([configImages.src], {since: gulp.lastRun('optimize:images')}) //Выберем наши картинки
            .pipe($.newer(configImages.dest))
            .pipe($.if(config.debug, $.debug({title: 'Static images:'})))
            .pipe($.imagemin(configImages.options))
            .pipe(gulp.dest(configImages.dest)); //выгрузим в build
    };
};
