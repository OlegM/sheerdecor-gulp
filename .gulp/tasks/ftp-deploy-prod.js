'use strict';

const $    = require('gulp-load-plugins')();
const gulp = require( 'gulp' );
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function () {
        var conn = ftp.create(config.deployProd.options);

        return gulp.src(config.deployProd.localFilesGlob, config.deployProd.srcOptions)
            .pipe($.plumber())
            .pipe($.if(config.debug, $.debug()))
            //.pipe(conn.newer(config.deployProd.remoteFolder)) // only upload newer files
            .pipe(conn.dest(config.deployProd.remoteFolder));
    };
};
