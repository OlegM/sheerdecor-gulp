'use strict';

const $           = require('gulp-load-plugins')();
const gulp        = require('gulp');
//const browsersync = require('browser-sync');

var onError = function (err) {
    $.util.beep();
    $.util.log(err);
    this.emit('end');
}; 

/**
 * Generate production CSS from SCSS
 * Build sourcemaps
 */
module.exports = function (config) {

    return function () {

        var sassConfig        = config.sass.options;
        var filesizeConfig    = config.size;
        var optimizeCssConfig = config.optimize.css.options;

        //sassConfig.onError = browsersync.notify;

        //browsersync.notify('Собираем CSS (SASS)');

        return gulp.src(config.sass.src)
            .pipe($.if(!config.production, $.sourcemaps.init()))
            .pipe($.plumber({errorHandler: onError}))
            .pipe($.sass(sassConfig).on('error', $.sass.logError))
            .pipe($.sass(sassConfig))
            //.pipe($.size(filesizeConfig))

            .pipe($.if(config.production, $.autoprefixer(config.autoprefixer)))
            .pipe($.if(config.production, $.cssmin(optimizeCssConfig.options)))
            .pipe($.if(!config.production, $.sourcemaps.write('.')))
            .pipe(gulp.dest(config.sass.dest))
            //.pipe($.size(filesizeConfig));
    };
};

