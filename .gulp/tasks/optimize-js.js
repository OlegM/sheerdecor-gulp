'use strict';

const $ = require('gulp-load-plugins')();
const gulp = require('gulp');

module.exports = function (config) {

  return function () {

    return gulp.src(config.optimize.js.src, { since: gulp.lastRun('optimize:js') })
      .pipe($.plumber())
      .pipe($.uglify())
      .pipe(gulp.dest(config.optimize.js.dest));
  };
};

