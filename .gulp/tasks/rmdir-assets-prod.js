'use strict';

const ftp    = require('vinyl-ftp');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function (cb) {
        var conn = ftp.create(config.deployProd.options);
        conn.rmdir(config.deployProd.deletedFolders, cb);
    }

};
