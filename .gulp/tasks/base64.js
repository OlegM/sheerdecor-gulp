'use strict';

const $    = require('gulp-load-plugins')();
const gulp = require('gulp');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function () {
        return gulp.src(config.base64.src)
            .pipe($.base64(config.base64.options))
            .pipe($.size(config.size))
            .pipe(gulp.dest(config.base64.dest));
    };
};
