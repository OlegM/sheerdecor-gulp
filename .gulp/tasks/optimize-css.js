'use strict';

const $  = require('gulp-load-plugins')();
var gulp = require('gulp');

/**
 * 1. Добавление вендорных префиксов
 * 3. Минификация CSS
 */
module.exports = function (config) {

    var filesizeConfig    = config.size;
    var optimizeCssConfig = config.optimize.css;

    return function () {
        return gulp.src(optimizeCssConfig.src)
            .pipe($.autoprefixer(config.autoprefixer))
            .pipe($.size(filesizeConfig))
            .pipe($.cssmin(optimizeCssConfig.options))
            .pipe($.replace('font-size:0;', 'font-size:0.001px;'))
            .pipe($.replace('font-size:0\}', 'font-size:0.001px\}'))
            .pipe(gulp.dest(optimizeCssConfig.dest))
            .pipe($.size(filesizeConfig));
        /*result.warnings().forEach(function (warn) {
            console.warn(warn.toString());
        });*/
    }
};