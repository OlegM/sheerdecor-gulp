'use strict';

const $         = require('gulp-load-plugins')();
const gulp      = require('gulp');

module.exports = function (config) {

    var configContentImages = config.optimize.contentImages;

    return function () {
        // Оптимизация статичных изображений (для верстки)
        return gulp.src([configContentImages.src], {since: gulp.lastRun('optimize:contentimages')}) //Выберем наши картинки
            .pipe($.newer(configContentImages.dest))
            .pipe($.if(config.debug, $.debug({title: 'Static images:'})))
            .pipe($.imagemin(config.optimize.images.options))
            .pipe(gulp.dest(configContentImages.dest)); //выгрузим в build
    };
};
