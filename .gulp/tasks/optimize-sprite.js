'use strict';

const $    = require('gulp-load-plugins')();
const gulp = require('gulp');

module.exports = function (config) {

    return function () {
        return gulp.src(config.sprites.dest + '/sprite-*.png', {since: gulp.lastRun('optimize:sprite')})
            .pipe($.newer(config.optimize.images.dest))
            .pipe($.imagemin())
            .pipe($.if(config.debug, $.debug({title: 'Sprite:'})))
            .pipe(gulp.dest(config.optimize.images.dest));
    }
};
