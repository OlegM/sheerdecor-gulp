'use strict';

const $       = require('gulp-load-plugins')();
const gulp    = require('gulp');
const shortid = require('shortid32');
const del     = require('del');

/**
 * Replace urls in CSS fies with base64 encoded data
 */
module.exports = function (config) {

    return function (callback) {

        shortid.characters(config.revCharacters);
        var spriteConfig   = config.sprites;
        var filesizeConfig = config.size;
        var spriteName     = 'icon';

        del([spriteConfig.dest + '/sprite-' + spriteName + '*.png'], {force: true}, function (err, paths) {
            gutil.log($.util.colors.red('Deleted file:\n', paths.join('\n')));
        });

        del([config.optimize.images.dest + '/sprite-' + spriteName + '*.png'], {force: true}, function (err, paths) {
            gutil.log($.util.colors.red('Deleted file:\n', paths.join('\n')));
        });

        var spriteImgName = 'sprite-' + spriteName + '-' + shortid.generate() + '.png';

        var spriteData =
                gulp.src(spriteConfig.src)
                    //.pipe($.debug())
                    .pipe($.spritesmith({
                        imgName: spriteImgName,
                        cssName: '_sprite-' + spriteName + '.scss',
                        cssFormat: 'scss',
                        imgPath: '../images/' + spriteImgName,
                        padding: 2,
                        algorithm: 'binary-tree',
                        cssVarMap: function (sprite) {
                            sprite.name = spriteName + '__' + sprite.name
                        },
                        cssOpts: {functions: false}
                    }));

        spriteData.img.pipe(gulp.dest(spriteConfig.dest))
            .pipe($.size(filesizeConfig));
        spriteData.css.pipe(gulp.dest(spriteConfig.cssDest));
        callback();
    }
};
