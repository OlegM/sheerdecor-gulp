'use strict';

const $         = require('gulp-load-plugins')();
const gulp      = require('gulp');
var browserSync = require('browser-sync');

module.exports = function (config) {

    return function () {
        return gulp.src(config.jade.src)
            .pipe($.plumber())
            .pipe($.jade(config.jade.options))
            .pipe($.if(config.debug, $.debug()))
            .pipe(gulp.dest(config.jade.dest))
            .on('end', browserSync.reload);
    }
};
