'use strict';

const $           = require('gulp-load-plugins')();
const gulp        = require('gulp');

var onError = function (err) {
    $.util.beep();
    $.util.log(err);
    this.emit('end');
};

module.exports = function (config) {

    return function () {

        var stylusConfig        = config.stylus.options;
        var filesizeConfig    = config.size;
        var optimizeCssConfig = config.optimize.css.options;

        return gulp.src(config.stylus.src)
            .pipe($.if(!config.production, $.sourcemaps.init()))
            .pipe($.plumber({errorHandler: onError}))
            .pipe($.stylus(stylusConfig))
            //.pipe($.size(filesizeConfig))

            .pipe($.if(config.production, $.autoprefixer(config.autoprefixer)))
            .pipe($.if(config.production, $.cssmin(optimizeCssConfig.options)))
            .pipe($.if(!config.production, $.sourcemaps.write('.')))
            .pipe(gulp.dest(config.stylus.dest))
    };
};


