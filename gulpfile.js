'use strict';
const config = require('./.gulp/config');
const gulp   = require('gulp');

lazyRequireTask('rmdir-assets-prod', './.gulp/tasks/rmdir-assets-prod');

lazyRequireTask('ftp-deploy', './.gulp/tasks/ftp-deploy');

lazyRequireTask('ftp-deploy-prod', './.gulp/tasks/ftp-deploy-prod');

lazyRequireTask('ftp-deploy-watch', './.gulp/tasks/ftp-deploy-watch');

lazyRequireTask('jade', './.gulp/tasks/jade');

lazyRequireTask('sass', './.gulp/tasks/sass');

lazyRequireTask('stylus', './.gulp/tasks/stylus');

lazyRequireTask('base64', './.gulp/tasks/base64');

lazyRequireTask('browsersync', './.gulp/tasks/browser-sync');

lazyRequireTask('sprite', './.gulp/tasks/sprite');

lazyRequireTask('clean:images', './.gulp/tasks/clean-images');

lazyRequireTask('optimize:css', './.gulp/tasks/optimize-css');

lazyRequireTask('build:js', './.gulp/tasks/build-js');

lazyRequireTask('optimize:js', './.gulp/tasks/optimize-js');

lazyRequireTask('optimize:images', './.gulp/tasks/optimize-images');

lazyRequireTask('optimize:sprite', './.gulp/tasks/optimize-sprite');

lazyRequireTask('optimize:contentimages', './.gulp/tasks/optimize-contentimages');

gulp.task('watch:styles', function () {
    gulp.watch(config.watch.sass, gulp.series(
      'sass',
      'optimize:css',
      'ftp-deploy-prod'
    ));
});

gulp.task('watch:js', function () {
    gulp.watch(config.watch.js, gulp.series(
      'build:js',
      'ftp-deploy-prod'
    ));
});

/*
gulp.task('watch:js', function () {
  gulp.watch(config.watch.js, gulp.series('build:js'));
});
*/


gulp.task('watch:sprite', function () {
    gulp.watch(config.watch.sprites, gulp.series('sprite', 'optimize:sprite'));
});

gulp.task('watch:images', function () {
    gulp.watch(config.watch.images, gulp.series('optimize:images'));
});

gulp.task('watch:contentimages', function () {
    gulp.watch(config.watch.contentImages, gulp.series('optimize:contentimages'));
});

gulp.task('watch:jade', function () {
    gulp.watch(config.watch.jade, gulp.series('jade'));
});

gulp.task('watch',
    gulp.series(
        gulp.parallel('jade', 'sass'),
        gulp.parallel('browsersync'),
        gulp.parallel(
            'watch:styles',
            'watch:js',
            'watch:sprite',
            'watch:jade',
            'watch:images',
            'watch:contentimages',
            //'ftp-deploy-watch',
            'ftp-deploy-prod'
        )
    )
);

gulp.task('default', gulp.series('watch'));

gulp.task('build:images',
    gulp.parallel(
        'optimize:images',
        'optimize:contentimages'
    )
);

gulp.task('build',
    gulp.series(
        'jade',
        gulp.series(
            'sprite',
            'sass',
            'base64'
        ),
        gulp.parallel(
            //'optimize:js',
            'optimize:css',
            'optimize:images',
            'optimize:contentimages'
        ),
        'ftp-deploy-prod'
    )
);

gulp.task('rebuild:images',
    gulp.series(
        'clean:images',
        'sprite',
        'sass',

        gulp.parallel(
            'optimize:images',
            'optimize:contentimages'
        )
    )
);


function lazyRequireTask(taskName, path) {
    config.taskName = taskName;
    gulp.task(taskName, function (callback) {
        var task = require(path).call(this, config);

        return task(callback);
    });
}
