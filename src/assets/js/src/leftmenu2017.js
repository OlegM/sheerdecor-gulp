;(function ($) {
    var leftmenu = document.querySelector('.lmenu');
    var leftmenuLinks = document.querySelectorAll('.lmenu:not(.leftmenu--mobile) > li >.lmenu__link');
    var leftmenuChildrens = document.querySelectorAll('.lmenu:not(.leftmenu--mobile) > .parent > .menu-v');
    var CLASS_OPEN = 'open';
    var SUBCATEGORY_LIST_CLASS = 'subcategory';
    var SUBCATEGORY_WRAP_CLASS = 'subcategory-wrap';
    var KEY_ESC = 27;
    var docBody = document.body;
    var isMenuActive = false;

    var leftmenuPositionTop,
        leftmenuPositionLeft,
        leftmenuPositionBottom;

    var subMenuPositionTop,
        subMenuPositionLeft,
        subMenuPositionBottom;

    var ASIDE_CLASS = 'aside';
    var ASIDE_WIDTH = 240;
    var COLUMN_WIDTH = 240;
    var LINK_PARENT_CLASS = 'lmenu__parent-link';
    var LINK_ACTIVE_CLASS = 'lmenu__link--active';
    var SIZE_LINK = 17;
    var WRAP_PADDING = 12;
    var leftmenuHeight = document.querySelector('.lmenu').clientHeight;
    var LEFTMENU_TOP = 120;

    var subCateggories;
    var activeLink;


    // Переменные для обработчиков событий
    var header = document.querySelector('.header');
    var mainarea = document.querySelector('.mainarea__content');

    /**
     * Задаем класс для родительских элементов выбранной ссылки
     */
    function setParentClassToLink() {
        var selectedElements = document.querySelectorAll('.lmenu .selected');

        if (selectedElements.length) {
            $('.selected').parents('.parent').each(function () {
                $(this).children('.lmenu__link').addClass('lmenu__link--parent');
            });
        }
    }

    /**
     * Устанавливаем переменную положения меню сверху и снизу
     * Вызывается при:
     * - инициализации
     * - изменении размера окна
     */
    function setMenuPositionTopBottom() {
        var positions = getMenuPosition();
        leftmenuPositionTop = positions.top;
        leftmenuPositionBottom = positions.bottom;
    }

    /**
     * Устанавливаем переменную положения меню слева
     * Вызывается при:
     * - инициализации
     * - изменении размера окна
     */
    function setMenuPositionLeft() {
        leftmenuPositionLeft = getMenuPosition().left;
    }

    /**
     * Хак: добавляем дополнительный класс подменю
     */
    function setleftmenuChildrenClass() {
        leftmenuChildrens.forEach(function (item) {
                item.classList.add(SUBCATEGORY_LIST_CLASS);
            }
        );
    }

    /**
     * Получаем массив подкатегорий
     */
    function getSubcategoriesArray() {
        if (!subCateggories) {
            subCateggories = document.querySelectorAll('.' + SUBCATEGORY_WRAP_CLASS);
        }
    }

    /**
     * Оборачиваем подменю в дополнительный враппер
     */
    function wrapLeftmenuChildrens() {
        if (!subCateggories) {
            leftmenuChildrens.forEach(function (children) {
                var wrapper = document.createElement('div');
                wrapper.className = SUBCATEGORY_WRAP_CLASS;
                children.parentNode.insertBefore(wrapper, children);
                wrapper.appendChild(children);
            });
        }
    }

    function getWindowHeight() {
        return window.innerHeight;
    }

    /**
     * Позиция меню
     * @returns {ClientRect}
     */
    function getMenuPosition() {
        return leftmenu.getBoundingClientRect();
    }

    /**
     * Положение активной родительской ссылки
     * @param el
     * @returns {ClientRect}
     */
    function getLinkPosition(el) {
        if (el) {
            return el.getBoundingClientRect();
        }
        return document.querySelector('.' + ASIDE_CLASS).getBoundingClientRect();
    }

    /**
     * Сброс положения блока подкатегорий
     * @param subcat
     */
    function resetSubcategoryPosition(subcat) {
        subcat.style.top = '';
        subcat.style.bottom = '';
    }

    /**
     * Удаляем класс OPEN у всех подкатегорий
     */
    function hideSubcategoryWrap(e) {
        var key;
        var event = e || window.event;

        if (event && event.type == 'keydown') {
            key = event.keyCode || event.which;
        }

        if (subCateggories && (key === KEY_ESC || key === undefined)) {
            subCateggories.forEach(function (subcategory) {
                subcategory.classList.remove(CLASS_OPEN);
            });
        }
    }

    function getSubcategoryTop(subcat) {
        return subcat.getBoundingClientRect().top;
    }

    /**
     * Задаем положение подменю
     */
    function setSubcategoriesWrapPosition() {
        if (subCateggories && subCateggories.length) {
            var height = leftmenuHeight < getWindowHeight() ? leftmenuHeight + 'px' : '100%';
            subMenuPositionTop = leftmenuPositionTop > 0 ? leftmenuPositionTop : 0;
            //subMenuPositionBottom = leftmenuPositionBottom > 0 ? '' : 0;
            subMenuPositionLeft = leftmenuPositionLeft + ASIDE_WIDTH;

            //console.log('leftmenuPositionBottom: ', leftmenuPositionBottom);

            subCateggories[0].style.height = height;
            subCateggories[0].style.top = subMenuPositionTop + 'px';
            subMenuPositionBottom = subCateggories[0].getBoundingClientRect().bottom;

            //console.log('subMenuPositionBottom: ', subMenuPositionBottom);
            subCateggories.forEach(function (subcategory) {
                subcategory.style.height = height;
                subcategory.style.maxHeight = height;
                subcategory.style.left = subMenuPositionLeft + 'px';
                if (subMenuPositionBottom >= getWindowHeight()) {
                    subcategory.style.bottom = '0px';
                    subcategory.style.top = '';
                } else {
                    subcategory.style.top = subMenuPositionTop + 'px';
                }
            });
        }
    }

    function removeGutters () {
        var gutters = document.querySelectorAll('.menu-gutter');

        if (gutters.length) {
            gutters.forEach(function (gutter) {
                gutter.parentNode.removeChild(gutter);
            })
        }
    }

    function categoryCols(count, category) {
        category.classList.remove('subcategory--1col');
        category.classList.remove('subcategory--2col');
        category.classList.remove('subcategory--3col');
        category.classList.remove('subcategory--4col');
        category.classList.remove('subcategory--5col');
        category.style.columnCount = count;
        category.style.MozColumnCount = count;
        category.style.webkitColumnCount = count;
        category.classList.add('subcategory--' + count + 'col');
    }

    function addCategoryGuters (category, gutterCount) {
        if (gutterCount > 1) {
            for (var i = 2; i <= gutterCount; i++) {
                var gutter = document.createElement('div');
                gutter.className = 'menu-gutter menu-gutter--' + i;
                category.parentNode.appendChild(gutter);
            }
        }
    }

    function setSubcategoryColumns(categoryWrap, subMenu) {
        //console.log(categoryWrap);
        var columnCount = 1;
        var subMenuHeight;

        if (categoryWrap) {
            var categoryWrapHeight = categoryWrap.offsetHeight;
            subMenuHeight = subMenu.offsetHeight;

            //console.log('wrapHeight: ' + wrapHeight);
            //console.log('categoryHeight: ' +categoryHeight);

            if (subMenuHeight > categoryWrapHeight && columnCount < 6) {
                removeGutters();

                for (columnCount; columnCount < 6; columnCount++) {
                    categoryCols(columnCount, subMenu);
                    subMenuHeight = subMenu.offsetHeight;
                    if (subMenuHeight < categoryWrapHeight) {
                        break;
                    }
                }

                addCategoryGuters(columnCount);
                //console.log(subMenuHeight, columnCount);
            }
        }
    }

    function setSubMenuPosition(link, category) {
        var linkPosition = getLinkPosition(link);
        var subMenu = category.firstChild;

        var subCategoryPositionTop, subCategoryPositionBottom, submenuHeight;

        if (subMenu) {
            subMenu.style.top = '';
            submenuHeight = subMenu.offsetHeight;

            //console.log('linkPosition.top: ', linkPosition.top);
            //console.log('submenuHeight / 2: ', submenuHeight / 2);

            if (submenuHeight + WRAP_PADDING * 2 >  category.offsetHeight) {
                setSubcategoryColumns(category, subMenu);
                submenuHeight = subMenu.offsetHeight;
            }

            subCategoryPositionTop = (linkPosition.top + SIZE_LINK) - WRAP_PADDING - subMenuPositionTop - (submenuHeight / 2);
            subCategoryPositionBottom = leftmenuHeight - submenuHeight - WRAP_PADDING * 2;
            //console.log('subCategoryPositionTop: ', subCategoryPositionTop);
            //console.log('subCategoryPositionBottom: ', subCategoryPositionBottom);
            //console.log('subMenuPositionTop: ', subMenuPositionTop);
            //console.log('subMenuPositionBottom: ', subMenuPositionBottom);
            //console.log('window.innerHeight: ', window.innerHeight);
            //subMenu.style.top = subCategoryPositionTop + 'px';
            //console.log('submenuHeight + subCategoryPositionTop + WRAP_PADDING * 2: ', submenuHeight + subCategoryPositionTop + WRAP_PADDING * 2);
            //console.log('==========================');

            // Если подкатегории вылазят за пределы подменю и подменю не прижато вверху
            if (subCategoryPositionTop < 0 && subMenuPositionTop > 0) {
                //console.log('подкатегории вылазят за пределы подменю и подменю не прижато вверху');
                subMenu.style.top = '-12px';
                // Если подкатегории вылазят за пределы подменю и подменю прижато вверху
            } else if (subCategoryPositionTop < 0) {
                //console.log('подкатегории вылазят за пределы подменю и подменю прижато вверху');
                subMenu.style.top = '0px';
                // Если подкатегории НЕ вылазят за пределы подменю
            } else if (subCategoryPositionTop > 0 && (submenuHeight + subCategoryPositionTop + WRAP_PADDING * 2 < leftmenuHeight)) {
                //console.log('подкатегории НЕ вылазят за пределы подменю');
                subMenu.style.top = subCategoryPositionTop + 'px';
                // Подкатегории вылазят за пределы меню внизу
            } else if (submenuHeight + subCategoryPositionTop + WRAP_PADDING * 2 > leftmenuHeight) {
                //console.log('Подкатегории вылазят за пределы меню внизу');
                if (subMenuPositionBottom >= window.innerHeight) {
                    subMenu.style.top = subCategoryPositionBottom + 'px';
                } else {
                    subMenu.style.top = subCategoryPositionBottom + WRAP_PADDING + 'px';
                }
            }
        }
    }

    /**
     * Добавление класса ссылкам верхнего уровня
     */
    function setClassToParentLinks() {
        if (leftmenuLinks) {
            leftmenuLinks.forEach(function (link) {
                link.classList.add(LINK_PARENT_CLASS);
            })
        }
    }

    /**
     * Удаление класса активной ссылки
     */
    function removeParentLinkActiveClass() {
        //var leftmenuActiveLinks = document.querySelectorAll(LINK_ACTIVE_CLASS);
        if (leftmenuLinks.length > 0) {
            leftmenuLinks.forEach(function (link) {
                link.classList.remove(LINK_ACTIVE_CLASS);
            })
        }
    }

    /**
     * Обработка hover на ссылках меню + раскрытие подменю
     * @param e - событие
     */
    function handleLinkHover(e) {
        var target = e.target;
        var link;
        var subcategoryWrap;

        if (target.classList.contains(LINK_PARENT_CLASS)) {
            link = target;
            subcategoryWrap = link.nextElementSibling;

            removeParentLinkActiveClass();
            hideSubcategoryWrap();
            link.classList.add(LINK_ACTIVE_CLASS);

            if (subcategoryWrap) {
                setSubMenuPosition(link, subcategoryWrap);
                subcategoryWrap.classList.add(CLASS_OPEN);
            }
            activeLink = link;
        }
    }

    function init() {
        setMenuPositionLeft();
        setMenuPositionTopBottom();
        setClassToParentLinks();
        setleftmenuChildrenClass();
        wrapLeftmenuChildrens();
        getSubcategoriesArray();
        setSubcategoriesWrapPosition();
        isMenuActive = true;
    }

    function initEvents() {
        addEvent(header, 'mouseover', hideSubcategoryWrap);
        addEvent(mainarea, 'mouseover', hideSubcategoryWrap);
        addEvent(docBody, 'keydown', hideSubcategoryWrap);
        addEvent(docBody, 'click', hideSubcategoryWrap);
        addEvent(leftmenu, 'mouseover', handleLinkHover);
        isMenuActive = true;
    }

    function destroyEvents() {
        removeEvent(header, 'mouseover', hideSubcategoryWrap);
        removeEvent(mainarea, 'mouseover', hideSubcategoryWrap);
        removeEvent(docBody, 'keydown', hideSubcategoryWrap);
        removeEvent(docBody, 'click', hideSubcategoryWrap);
        removeEvent(leftmenu, 'mouseover', handleLinkHover);

        $('.' + SUBCATEGORY_WRAP_CLASS).css({ left: '', height: '' });
        isMenuActive = false;
    }

    $(window).resize(function () {
        if (window.innerWidth > 1024) {
            if (!isMenuActive) {
                init();
                initEvents();
            }
            setMenuPositionLeft();
            setSubcategoriesWrapPosition();
            //setSubcategoryColumns(activeLink);
        } else {
            destroyEvents();
        }
    });

    window.onscroll = function () {
        if (window.innerWidth > 1024) {
            setMenuPositionTopBottom();
            setSubcategoriesWrapPosition();
        }
    };

    $(document).ready(function () {
        if (window.innerWidth > 1024) {
            init();
            initEvents();
        }
        setParentClassToLink();
    });

    // Хелперы событий для совместимости с IE8
    function addEvent(el, type, handler) {
        if (el.attachEvent) el.attachEvent('on' + type, handler); else el.addEventListener(type, handler);
    }

    function removeEvent(el, type, handler) {
        if (el.detachEvent) el.detachEvent('on' + type, handler); else el.removeEventListener(type, handler);
    }

})(jQuery);
