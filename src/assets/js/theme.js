var _extends = Object.assign || function (e) {
  for (var t = 1; t < arguments.length; t++) {
    var n = arguments[t];
    for (var o in n) Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o])
  }
  return e
}, _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
  return typeof e
} : function (e) {
  return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
};
!function (e, t) {
  "object" === ("undefined" == typeof exports ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.LazyLoad = t()
}(this, function () {
  "use strict";
  var e = {
    elements_selector: "img",
    container: window,
    threshold: 300,
    throttle: 150,
    data_src: "original",
    data_srcset: "originalSet",
    class_loading: "loading",
    class_loaded: "loaded",
    class_error: "error",
    class_initial: "initial",
    skip_invisible: !0,
    callback_load: null,
    callback_error: null,
    callback_set: null,
    callback_processed: null
  }, t = !("onscroll" in window) || /glebot/.test(navigator.userAgent), n = function (e, t) {
    e && e(t)
  }, o = function (e) {
    return e.getBoundingClientRect().top + window.pageYOffset - e.ownerDocument.documentElement.clientTop
  }, i = function (e, t, n) {
    return (t === window ? window.innerHeight + window.pageYOffset : o(t) + t.offsetHeight) <= o(e) - n
  }, s = function (e) {
    return e.getBoundingClientRect().left + window.pageXOffset - e.ownerDocument.documentElement.clientLeft
  }, l = function (e, t, n) {
    var o = window.innerWidth;
    return (t === window ? o + window.pageXOffset : s(t) + o) <= s(e) - n
  }, r = function (e, t, n) {
    return (t === window ? window.pageYOffset : o(t)) >= o(e) + n + e.offsetHeight
  }, a = function (e, t, n) {
    return (t === window ? window.pageXOffset : s(t)) >= s(e) + n + e.offsetWidth
  }, c = function (e, t, n) {
    return !(i(e, t, n) || r(e, t, n) || l(e, t, n) || a(e, t, n))
  }, u = function (e, t) {
    var n = new e(t), o = new CustomEvent("LazyLoad::Initialized", { detail: { instance: n } });
    window.dispatchEvent(o)
  }, d = function (e, t) {
    var n = e.parentElement;
    if ("PICTURE" === n.tagName) for (var o = 0; o < n.children.length; o++) {
      var i = n.children[o];
      if ("SOURCE" === i.tagName) {
        var s = i.dataset[t];
        s && i.setAttribute("srcset", s)
      }
    }
  }, f = function (e, t, n) {
    var o = e.tagName, i = e.dataset[n];
    if ("IMG" === o) {
      d(e, t);
      var s = e.dataset[t];
      return s && e.setAttribute("srcset", s), void(i && e.setAttribute("src", i))
    }
    return "IFRAME" === o ? void(i && e.setAttribute("src", i)) : void(i && (e.style.backgroundImage = "url(" + i + ")"))
  }, m = function (t) {
    this._settings = _extends({}, e, t), this._queryOriginNode = this._settings.container === window ? document : this._settings.container, this._previousLoopTime = 0, this._loopTimeout = null, this._boundHandleScroll = this.handleScroll.bind(this), this._isFirstLoop = !0, window.addEventListener("resize", this._boundHandleScroll), this.update()
  };
  m.prototype = {
    _reveal: function (e) {
      var t = this._settings, o = function s() {
        t && (e.removeEventListener("load", i), e.removeEventListener("error", s), e.classList.remove(t.class_loading), e.classList.add(t.class_error), n(t.callback_error, e))
      }, i = function l() {
        t && (e.classList.remove(t.class_loading), e.classList.add(t.class_loaded), e.removeEventListener("load", l), e.removeEventListener("error", o), n(t.callback_load, e))
      };
      "IMG" !== e.tagName && "IFRAME" !== e.tagName || (e.addEventListener("load", i), e.addEventListener("error", o), e.classList.add(t.class_loading)), f(e, t.data_srcset, t.data_src), n(t.callback_set, e)
    }, _loopThroughElements: function () {
      var e = this._settings, o = this._elements, i = o ? o.length : 0, s = void 0, l = [], r = this._isFirstLoop;
      for (s = 0; s < i; s++) {
        var a = o[s];
        e.skip_invisible && null === a.offsetParent || (t || c(a, e.container, e.threshold)) && (r && a.classList.add(e.class_initial), this._reveal(a), l.push(s), a.dataset.wasProcessed = !0)
      }
      for (; l.length > 0;) o.splice(l.pop(), 1), n(e.callback_processed, o.length);
      0 === i && this._stopScrollHandler(), r && (this._isFirstLoop = !1)
    }, _purgeElements: function () {
      var e = this._elements, t = e.length, n = void 0, o = [];
      for (n = 0; n < t; n++) e[n].dataset.wasProcessed && o.push(n);
      for (; o.length > 0;) e.splice(o.pop(), 1)
    }, _startScrollHandler: function () {
      this._isHandlingScroll || (this._isHandlingScroll = !0, this._settings.container.addEventListener("scroll", this._boundHandleScroll))
    }, _stopScrollHandler: function () {
      this._isHandlingScroll && (this._isHandlingScroll = !1, this._settings.container.removeEventListener("scroll", this._boundHandleScroll))
    }, handleScroll: function () {
      var e = this._settings.throttle;
      if (0 !== e) {
        var t = function () {
          (new Date).getTime()
        }, n = t(), o = e - (n - this._previousLoopTime);
        o <= 0 || o > e ? (this._loopTimeout && (clearTimeout(this._loopTimeout), this._loopTimeout = null), this._previousLoopTime = n, this._loopThroughElements()) : this._loopTimeout || (this._loopTimeout = setTimeout(function () {
          this._previousLoopTime = t(), this._loopTimeout = null, this._loopThroughElements()
        }.bind(this), o))
      } else this._loopThroughElements()
    }, update: function () {
      this._elements = Array.prototype.slice.call(this._queryOriginNode.querySelectorAll(this._settings.elements_selector)), this._purgeElements(), this._loopThroughElements(), this._startScrollHandler()
    }, destroy: function () {
      window.removeEventListener("resize", this._boundHandleScroll), this._loopTimeout && (clearTimeout(this._loopTimeout), this._loopTimeout = null), this._stopScrollHandler(), this._elements = null, this._queryOriginNode = null, this._settings = null
    }
  };
  var h = window.lazyLoadOptions;
  return h && function (e, t) {
    var n = t.length;
    if (n) for (var o = 0; o < n; o++) u(e, t[o]); else u(e, t)
  }(m, h), m
}), function (e) {
  function t() {
    if (window.innerWidth < 756) {
      var t = e(window).scrollTop();
      t >= 94 ? s.addClass("header--fixed") : s.removeClass("header--fixed")
    }
  }

  function n(e) {
    s.hasClass("header--fixed") ? e.css("max-height", window.innerHeight - 35 + "px") : e.css("max-height", window.innerHeight - 127 + "px")
  }

  function o() {
    var t = e(".category__products"), n = e(".category__submenu-wrap");
    e(".category-submenu--mobile").length || t.append(n.clone().addClass("category-submenu--mobile"))
  }

  function i() {
    window.innerWidth < 800 && o()
  }

  var s = e(".header");
  t(), e(document).ready(function () {
    /*setTimeout(function () {
      mgo && e(".company-phone").removeClass("company-phone--hidden")
    }, 300),*/ t(), e(window).scroll(function () {
      t()
    }), e(".tab__link").on("click", function (t) {
      t.preventDefault(), t.stopPropagation(), e(".tab__link").not(this).removeClass("active"), e(this).toggleClass("active"), e("body").on("click", function () {
        e(".tab__link").removeClass("active")
      })
    });
    var s = e(".lmenu"), l = e(".mobile-nav"), r = e(".mobile-nav-overlay"), a = e(".mobile-nav-toggle"),
      c = e(".bd-bg");
    e("#mobile-nav-toggle").click(function (t) {
      t.preventDefault(), t.stopPropagation(), l.toggleClass("mobile-nav--open"), a.toggleClass("mobile-nav-toggle--active"), l.hasClass("mobile-nav--open") ? (n(l), c.css("overflow-y", "hidden"), r.css("display", "block")) : (l.css("max-height", 0), c.css("overflow-y", ""), r.css("display", "")), e(".leftmenu--mobile").length || l.prepend(s.clone().addClass("leftmenu--mobile"))
    }), e(window).resize(function () {
      window.innerWidth > 1024 && e(".leftmenu--mobile").length && (l.removeClass("leftmenu--mobile"), e(".leftmenu--mobile").remove()), window.innerWidth < 800 && !e(".category-submenu--mobile").length && o(), t(), l.hasClass("mobile-nav--open") && n(l)
    }), e(document).on("click", function () {
      l.removeClass("mobile-nav--open"), a.removeClass("mobile-nav-toggle--active"), c.css("overflow-y", ""), r.css("display", "")
    });
    var u = document.forms.search;
    e(u).on("submit", function (e) {
      e.preventDefault(), u.elements.query.value.length < 3 || u.submit()
    }), i()
  })
}(jQuery), function (e) {
  function t() {
    var t = document.querySelectorAll(".lmenu .selected");
    t.length && e(".selected").parents(".parent").each(function () {
      e(this).children(".lmenu__link").addClass("lmenu__link--parent")
    })
  }

  function n() {
    var e = a();
    S = e.top, H = e.bottom
  }

  function o() {
    k = a().left
  }

  function i() {
    z.forEach(function (e) {
      e.classList.add(B)
    })
  }

  function s() {
    A || (A = document.querySelectorAll("." + I))
  }

  function l() {
    A || z.forEach(function (e) {
      var t = document.createElement("div");
      t.className = I, e.parentNode.insertBefore(t, e), t.appendChild(e)
    })
  }

  function r() {
    return window.innerHeight
  }

  function a() {
    return N.getBoundingClientRect()
  }

  function c(e) {
    return e ? e.getBoundingClientRect() : document.querySelector("." + F).getBoundingClientRect()
  }

  function u(e) {
    var t, n = e || window.event;
    n && "keydown" == n.type && (t = n.keyCode || n.which), !A || t !== P && void 0 !== t || A.forEach(function (e) {
      e.classList.remove(W)
    })
  }

  function d() {
    if (A && A.length) {
      var e = U < r() ? U + "px" : "100%";
      T = S > 0 ? S : 0, x = k + M, A[0].style.height = e, A[0].style.top = T + "px", q = A[0].getBoundingClientRect().bottom, A.forEach(function (t) {
        t.style.height = e, t.style.maxHeight = e, t.style.left = x + "px", q >= r() ? (t.style.bottom = "0px", t.style.top = "") : t.style.top = T + "px"
      })
    }
  }

  function f() {
    var e = document.querySelectorAll(".menu-gutter");
    e.length && e.forEach(function (e) {
      e.parentNode.removeChild(e)
    })
  }

  function m(e, t) {
    t.classList.remove("subcategory--1col"), t.classList.remove("subcategory--2col"), t.classList.remove("subcategory--3col"), t.classList.remove("subcategory--4col"), t.classList.remove("subcategory--5col"), t.style.columnCount = e, t.style.MozColumnCount = e, t.style.webkitColumnCount = e, t.classList.add("subcategory--" + e + "col")
  }

  function h(e, t) {
    if (t > 1) for (var n = 2; n <= t; n++) {
      var o = document.createElement("div");
      o.className = "menu-gutter menu-gutter--" + n, e.parentNode.appendChild(o)
    }
  }

  function p(e, t) {
    var n, o = 1;
    if (e) {
      var i = e.offsetHeight;
      if (n = t.offsetHeight, n > i && o < 6) {
        for (f(), o; o < 6 && (m(o, t), n = t.offsetHeight, !(n < i)); o++) ;
        h(o)
      }
    }
  }

  function g(e, t) {
    var n, o, i, s = c(e), l = t.firstChild;
    l && (l.style.top = "", i = l.offsetHeight, i + 2 * Q > t.offsetHeight && (p(t, l), i = l.offsetHeight), n = s.top + G - Q - T - i / 2, o = U - i - 2 * Q, n < 0 && T > 0 ? l.style.top = "-12px" : n < 0 ? l.style.top = "0px" : n > 0 && i + n + 2 * Q < U ? l.style.top = n + "px" : i + n + 2 * Q > U && (q >= window.innerHeight ? l.style.top = o + "px" : l.style.top = o + Q + "px"))
  }

  function v() {
    R && R.forEach(function (e) {
      e.classList.add(X)
    })
  }

  function _() {
    R.length > 0 && R.forEach(function (e) {
      e.classList.remove(Y)
    })
  }

  function y(e) {
    var t, n, o = e.target;
    o.classList.contains(X) && (t = o, n = t.nextElementSibling, _(), u(), n && (g(t, n), n.classList.add(W)), O = t)
  }

  function w() {
    o(), n(), v(), i(), l(), s(), d(), j = !0
  }

  function b() {
    E(J, "mouseover", u), E(K, "mouseover", u), E(D, "keydown", u), E(D, "click", u), E(N, "mouseover", y), j = !0
  }

  function L() {
    C(J, "mouseover", u), C(K, "mouseover", u), C(D, "keydown", u), C(D, "click", u), C(N, "mouseover", y), e("." + I).css({
      left: "",
      height: ""
    }), j = !1
  }

  function E(e, t, n) {
    e.attachEvent ? e.attachEvent("on" + t, n) : e.addEventListener(t, n)
  }

  function C(e, t, n) {
    e.detachEvent ? e.detachEvent("on" + t, n) : e.removeEventListener(t, n)
  }

  var S, k, H, T, x, q, A, O, N = document.querySelector(".lmenu"),
    R = document.querySelectorAll(".lmenu:not(.leftmenu--mobile) > li >.lmenu__link"),
    z = document.querySelectorAll(".lmenu:not(.leftmenu--mobile) > .parent > .menu-v"), W = "open", B = "subcategory",
    I = "subcategory-wrap", P = 27, D = document.body, j = !1, F = "aside", M = 240, X = "lmenu__parent-link",
    Y = "lmenu__link--active", G = 17, Q = 12, U = document.querySelector(".lmenu").clientHeight,
    J = document.querySelector(".header"), K = document.querySelector(".mainarea__content");
  e(window).resize(function () {
    window.innerWidth > 1024 ? (j || (w(), b()), o(), d()) : L()
  }), window.onscroll = function () {
    window.innerWidth > 1024 && (n(), d())
  }, e(document).ready(function () {
    window.innerWidth > 1024 && (w(), b()), t()
  })
}(jQuery);