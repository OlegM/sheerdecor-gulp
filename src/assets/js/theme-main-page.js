$(document).ready(function () {
  $('#home_list .prod').on('click', function (e) {
    var categoryUrl = $(this).closest('.category-block').find('.category-title').attr('href');
    //window.location = categoryUrl;
    setTimeout(function() {
      document.location.href = categoryUrl;
    }, 300);
  });

  $('#home_list .prod__img').removeAttr('title');
});
