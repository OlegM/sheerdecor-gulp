(function ($) {
    var leftmenuLinks          = $(".lmenu.menu-v > .parent>.lmenu__link");
    var leftmenuChildren       = $('.lmenu > .parent > .menu-v');
    var CLASS_OPEN             = 'open';
    var SUBCATEGORY_LIST_CLASS = 'subcategory';
    var SUBCATEGORY_WRAP_CLASS = 'subcategory-wrap';
    var ASIDE_CLASS            = 'aside';
    var ASIDE_WIDTH            = 240;
    var COLUMN_WIDTH           = 240;

    var $subCateggories;

    function setSubcategoryPosition() {
        var asidePosition = $('.' + ASIDE_CLASS).offset();
        $subCateggories.css('left', asidePosition.left + ASIDE_WIDTH + 'px');
    }

    function setSubcategoryColumns() {
        console.log('=================');

        function setColumns(el) {
            var wrapHeight    = $('.' + SUBCATEGORY_WRAP_CLASS + ':eq(0)').height();
            var columnCount   = 1;
            var $categoryList = $(el).children('.' + SUBCATEGORY_LIST_CLASS);

            $categoryList.css({'column-count': columnCount, 'width': columnCount * COLUMN_WIDTH + 'px'});

            while ($categoryList.innerHeight() > wrapHeight) {
                columnCount++;
                $categoryList.css({'column-count': columnCount, 'width': columnCount * COLUMN_WIDTH + 'px'});
            }
        }

        $subCateggories.each(function (index, el) {
            setColumns(el);
        });
    }

    function init() {
        $(leftmenuChildren).addClass(SUBCATEGORY_LIST_CLASS);
        $(leftmenuChildren).wrap('<div class="' + SUBCATEGORY_WRAP_CLASS + '"></div>');
        $subCateggories = $('.' + SUBCATEGORY_WRAP_CLASS);
        setSubcategoryPosition();
        setSubcategoryColumns();
    }

    leftmenuLinks.mouseenter(
        function () {
            var self            = this;
            var selfNextSibling = self.nextSibling;

            $subCateggories.each(function (index, el) {
                if (el != selfNextSibling) {
                    $(el).removeClass(CLASS_OPEN);
                }
            });

            setSubcategoryColumns();
            $(selfNextSibling).addClass(CLASS_OPEN);
        });

    $('.mainarea__content').mouseover(function () {
        $('.subcategory-wrap.open').removeClass(CLASS_OPEN);
    });

    $('.bd-bg').keydown(function (ev) {
        if (ev.which == 27) {
            $('.subcategory-wrap.open').removeClass(CLASS_OPEN);
        }
    });

    $(document).on('click', function () {
        $subCateggories.removeClass(CLASS_OPEN);
    });

    $(window).resize(function () {
        if ($('body').innerWidth() > 800) {
            setSubcategoryPosition();
            //setSubcategoryColumns();
        }
    });


    $(document).ready(function () {
        init();
    });


})(jQuery);