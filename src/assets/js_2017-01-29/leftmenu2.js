;(function ($) {

    var leftmenuLinks          = $(".lmenu:not(.leftmenu--mobile) > li >.lmenu__link");
    var leftmenu = $('.lmenu');
    var SIZE_LINK = 17;
    var WRAP_PADDING = 12;

    function setMenuPosition(el) {
        var linkPosition = el.getBoundingClientRect();
        var categoryWrap = el.nextSibling;
        var windowHeight = window.innerHeight;

        var category;
        if (categoryWrap) {
            category           = categoryWrap.firstChild;
            var categoryHeight = category.offsetHeight;
            var categoryTop    = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2;

            var categoryWrapHeight = categoryWrap.offsetHeight;
            console.log(windowHeight, categoryWrapHeight);
            if (windowHeight < categoryWrapHeight) {
                categoryWrap.style.top = '0px';
                categoryWrap.style.bottom = '0px';
            }

            if (categoryTop < 0) {
                category.style.top = '';
            } else if (categoryTop > 0 || (categoryTop + category.offsetHeight + WRAP_PADDING * 2) < windowHeight) {
                //category.style.top = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2 + 'px';
            }
            if ((categoryTop + category.offsetHeight + WRAP_PADDING * 2) > windowHeight) {
                //category.style.top = windowHeight - category.offsetHeight - WRAP_PADDING * 2 + 'px';
            }
        }
    }

    leftmenuLinks.mouseenter(
        function () {
            setMenuPosition(this);
            /*var linkPosition = this.getBoundingClientRect();
            //console.log(linkPosition);
            div.innerHTML    = 'top: ' + linkPosition.top + '; bottom: ' + linkPosition.bottom;
            document.body.appendChild(div);

            var categoryWrap = this.nextSibling;
            var windowHeight = window.innerHeight;

            var category;
            if (categoryWrap) {
                category           = categoryWrap.firstChild;
                var categoryPos    = category.getBoundingClientRect();
                var categoryHeight = category.offsetHeight;
                var categoryTop    = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2;
                //console.log(categoryTop, windowHeight, categoryTop + category.offsetHeight + WRAP_PADDING);
                //console.log(categoryPos.top, categoryHeight, (categoryHeight / 2 + WRAP_PADDING), ' | ', categoryPos.bottom, categoryHeight, (categoryHeight / 2 + 12));

                if (categoryTop < 0) {
                    category.style.top = '';
                } else if (categoryTop > 0 || (categoryTop + category.offsetHeight + WRAP_PADDING * 2) < windowHeight) {
                    category.style.top = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2 + 'px';
                }
                if ((categoryTop + category.offsetHeight + WRAP_PADDING * 2) > windowHeight) {
                    category.style.top = windowHeight - category.offsetHeight + WRAP_PADDING + 'px';
                }
                /!*console.log(categoryTop > 0);
                console.log((categoryTop + category.offsetHeight + 24) < windowHeight);
                if (categoryTop > 0 || (categoryTop + category.offsetHeight + 24) < windowHeight) {
                    if (categoryTop > 0 && (categoryTop + category.offsetHeight + 24) > windowHeight)
                    category.style.top = linkPosition.top + 16 - 12 - categoryHeight / 2 + 'px';
                }*!/
            }*/
            var self = this;

            $(window).resize(function() {
                setMenuPosition(self);
            });
        });

})(jQuery);