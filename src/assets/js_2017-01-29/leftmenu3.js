;(function ($) {
    var leftmenuLinks          = $(".lmenu:not(.leftmenu--mobile) > li >.lmenu__link");
    var leftmenuChildren       = $('.lmenu:not(.leftmenu--mobile) > .parent > .menu-v');
    var CLASS_OPEN             = 'open';
    var SUBCATEGORY_LIST_CLASS = 'subcategory';
    var SUBCATEGORY_WRAP_CLASS = 'subcategory-wrap';
    var ASIDE_CLASS            = 'aside';
    var ASIDE_WIDTH            = 240;
    var COLUMN_WIDTH           = 240;
    var isMenuActive           = false;
    var LINK_ACTIVE_CLASS      = 'lmenu__link--active';
    var SIZE_LINK              = 18;
    var WRAP_PADDING           = 12;

    var $subCateggories;
    var activeLink;


    function setSubcategoryPosition() {
        var asidePosition = $('.' + ASIDE_CLASS).offset();
        $subCateggories.css('left', asidePosition.left + ASIDE_WIDTH + 'px');
    }

    function setSubcategoryColumns(element) {
        var categoryWrap = element.nextElementSibling;
        var columnCount  = 1;

        //$subCateggories.css('max-height', $('.'+SUBCATEGORY_WRAP_CLASS).height())


        function categoryCols(count) {
            $('.menu-gutter').remove();
            category.classList.remove('subcategory--1col');
            category.classList.remove('subcategory--2col');
            category.classList.remove('subcategory--3col');
            category.classList.remove('subcategory--4col');
            category.classList.remove('subcategory--5col');
            category.style.columnCount = count;
            //category.style.width       = COLUMN_WIDTH * count + 'px';
            category.classList.add('subcategory--' + count + 'col');
            categoryHeight = category.offsetHeight;

            if (count > 1) {
                for (var i = 2; i <= count; i++) {
                    var gutter       = document.createElement('div');
                    gutter.className = 'menu-gutter menu-gutter--' + i;
                    category.parentNode.appendChild(gutter);
                }
            }
        }

        if (categoryWrap !== null) {
            var wrapHeight     = categoryWrap.offsetHeight;
            var category       = categoryWrap.firstChild;
            var categoryHeight = category.offsetHeight;

            categoryCols(columnCount);

            while (categoryHeight > wrapHeight) {
                categoryCols(columnCount);
                columnCount++;
            }

            if (category) {
                setMenuPosition(category);
            }
        }
    }

    function setMenuPosition(el) {
        var linkPosition         = el.getBoundingClientRect();
        var categoryWrap         = el.parentNode;
        var categoryWrapPosition = categoryWrap.getBoundingClientRect();
        var windowHeight         = window.innerHeight;
        var category             = el;

        console.log(category);

        if (categoryWrap) {
            var categoryHeight     = category.offsetHeight;
            var categoryTop        = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2;
            var categoryWrapHeight = categoryWrap.offsetHeight;

            console.log(categoryWrapPosition);

            if (windowHeight <= categoryWrapHeight) {
                categoryWrap.style.top    = '0px';
                categoryWrap.style.bottom = '0px';
            } else {
                categoryWrap.style.top    = '';
                categoryWrap.style.bottom = '';
            }


            if (categoryTop < 0) {
                category.style.top = '';
            } else if (categoryTop > 0 || (categoryTop + category.offsetHeight + WRAP_PADDING * 2) < windowHeight) {
                category.style.top = linkPosition.top + SIZE_LINK - WRAP_PADDING - categoryHeight / 2 + 'px';
            }
            if ((categoryTop + category.offsetHeight + WRAP_PADDING * 2) > windowHeight) {
                category.style.top = windowHeight - category.offsetHeight - WRAP_PADDING * 2 + 'px';
            }
        }
    }

    function init() {
        var leftmenuHeight = $('.lmenu').innerHeight();

        $(leftmenuChildren).addClass(SUBCATEGORY_LIST_CLASS);
        if (!$subCateggories) {
            $(leftmenuChildren).wrap('<div class="' + SUBCATEGORY_WRAP_CLASS + '"></div>');
        }
        $subCateggories = $('.' + SUBCATEGORY_WRAP_CLASS);
        isMenuActive    = true;

        $subCateggories.css('max-height', leftmenuHeight);
    }

    function handleLinks() {

        leftmenuLinks.mouseover(
            function () {
                var selfNextSibling = $(this).siblings('.' + SUBCATEGORY_WRAP_CLASS);
                leftmenuLinks.removeClass(LINK_ACTIVE_CLASS);
                $(self).addClass(LINK_ACTIVE_CLASS);

                $subCateggories.each(function (index, el) {
                    if (el != selfNextSibling) {
                        $(el).removeClass(CLASS_OPEN);
                    }
                });
                setSubcategoryColumns(this);
                activeLink = this;
                $(selfNextSibling).addClass(CLASS_OPEN);
            });
    }

    function destroyEvents() {
        leftmenuLinks.unbind('mouseenter');
        $('.bd-bg').unbind('keydown');
        $('.mainarea__content').unbind('mouseover');

        isMenuActive = false;
    }

    function initEvents() {
        $('.mainarea__content, .header').mouseover(function () {
            $('.subcategory-wrap.open').removeClass(CLASS_OPEN);
        });

        $('.bd-bg').keydown(function (ev) {
            if (ev.which == 27) {
                $('.subcategory-wrap.open').removeClass(CLASS_OPEN);
            }
        });

        $(document).on('click', function () {
            $('.subcategory-wrap.open').removeClass(CLASS_OPEN);
        });

        handleLinks();

        isMenuActive = true;
    }


    $(window).resize(function () {
        if (window.innerWidth > 1024) {
            if (!isMenuActive) {
                init();
                initEvents();
            }
            setSubcategoryPosition();
            setSubcategoryColumns(activeLink);
        } else {
            destroyEvents();
        }

        /*var self = this;

         $(window).resize(function () {
         setMenuPosition(self);
         });*/
    });


    $(document).ready(function () {
        if (window.innerWidth > 1024) {
            init();
            initEvents();
            handleLinks();
            setSubcategoryPosition();
        }

        if ($('.lmenu .selected').length) {

            $('.selected').parents('.parent').each(function () {
                $(this).children('.lmenu__link').addClass('lmenu__link--parent');
            });
        }
    });


})(jQuery);